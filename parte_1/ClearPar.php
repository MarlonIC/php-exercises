<?php
/**
* Author: Marlon Inga Cahuana
* email: marlonricslim@gmail.com
*/
class ClearPar
{
	
	public function build(string $cadena) {
		$arr_cad = str_split($cadena);
		$str_out = '';
		$num = 0;
		while (count($arr_cad) > $num) {
			if (isset($arr_cad[$num+1]) and $arr_cad[$num] . $arr_cad[$num+1] == '()') {
				$str_out .= $arr_cad[$num] . $arr_cad[$num+1];
				$num++;
			}
			$num++;
		}
		echo $str_out . '<br>';
	}
}

$obj = new ClearPar();
$obj->build('()())()');
$obj->build('()(()');
$obj->build(')(');
$obj->build('((()');

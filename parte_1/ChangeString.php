<?php
/**
* Author: Marlon Inga Cahuana
* email: marlonricslim@gmail.com
*/
class ChangeString
{
	public function build(string $cadena) {
		# $replace = preg_replace(['/a/'], ['b'], $cadena);
		$replace = str_split($cadena);
		foreach ($replace as $idx => $letter) {
			if (preg_match('/[a-z]|[A-Z]/', $letter)) {
				if ($letter == 'Z') { $letter = '@'; }
				if ($letter == 'z') { $letter = '`'; }
				$replace[$idx] = chr(ord($letter)+1);
			}
		}
		echo implode($replace) . "<br>";
	}
}

$obj = new ChangeString();
$obj->build('123 abcd*3');
$obj->build('**Casa 52');
$obj->build('**Casa 52Z');
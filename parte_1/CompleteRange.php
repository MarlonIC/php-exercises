<?php
/**
* Author: Marlon Inga Cahuana
* email: marlonricslim@gmail.com
*/
class CompleteRange
{
	
	public function build(array $collection) {
		$happend = TRUE;
		foreach ($collection as $idx => $val) {
			if (array_key_exists($idx + 1, $collection) and !($val < $collection[$idx + 1])) {
				$happend = FALSE;
			}
		}

		if (!$happend) {
			echo "Ingrese un array con orden ascendente!<br>";
			return FALSE;
		}

		$arr_cll = [];
		
		/* Print result */
		echo '[';
		$iter = range($collection[0], $collection[count($collection)-1]);
		foreach ($iter as $idx => $num) {
			if ($idx + 1 == count($iter)) { echo $num; }
			else { echo sprintf('%d, ', $num); } 
		}
		echo ']<br>';
		
	}
}

$obj = new CompleteRange();
$obj->build([1, 2, 4, 5]);
$obj->build([2, 4, 9]);
$obj->build([55, 58, 60]);
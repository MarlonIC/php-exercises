<?php
use Slim\Views\PhpRenderer;

require 'vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$container = $app->getContainer();
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('view', [
        'cache' => FALSE #'var/cache'
    ]);

    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

    return $view;
};

// View main
$app->map(['GET', 'POST'], '/', function ($request, $response, $args) {
	$employees = file_get_contents('bd/employees.json');
	$employees = json_decode($employees);

	$email = $request->getParam('email');

	if ($email != '') {
		$employees = array_filter($employees, function($emp) use ($email) {
			if (!is_bool(strpos($emp->email, $email))) {
				return $emp;
			}
		});
	}

	$data['employees'] = $employees;
	$data['email'] = $email;
	return $this->view->render($response, 'index.php', $data);
});


$app->get('/detalle/{id}', function($request, $response, $args) {
	$employees = file_get_contents('bd/employees.json');
	$employees = json_decode($employees);

	$employee = array_filter($employees, function($emp) use ($args) {
		if ($emp->id == $args['id']) { return $emp; }
	});
	$data['employee'] = $employee;
	return $this->view->render($response, 'detalle.php', $data);
});

$app->get('/registrar', function($request, $response, $args) {
	$skills = ['PHP', 'C#', 'C++', 'Lisp', 'Perl', 'Ruby', 'JS'];
	$data['skills'] = $skills;

	return $this->view->render($response, 'registrar.php', $data);
});

$app->post('/registrarsave', function($request, $response, $args) {
	$data['id'] = substr(md5(uniqid(rand(), true)), 0, 24);
	$data['isOnline'] = $request->getParam('isOnline') == 'S' ? True : False;
	$data['salary'] = '$' . number_format((float)$request->getParam('salary'), 2);
	$data['age'] = $request->getParam('age');
	$data['position'] = $request->getParam('position');
	$data['name'] = $request->getParam('name');
	$data['gender'] = $request->getParam('gender');
	$data['email'] = $request->getParam('email');
	$data['phone'] = $request->getParam('phone');
	$data['address'] = $request->getParam('address');
	$skills = $request->getParam('skills');
	$tmp_skills = [];
	foreach ($skills as $val) {
		array_push($tmp_skills, (object)['skill' => $val]);
	}
	$data['skills'] = $tmp_skills;

	$employees = file_get_contents('bd/employees.json');
	$employees = json_decode($employees);
	array_push($employees, $data);

	$json = fopen('bd/employees.json', 'w');
	fwrite($json, json_encode($employees));
	fclose($json);

	return $response->withRedirect('/');
});

$app->get('/service[/{min}/{max}]', function($request, $response, $args) {
	$employees = file_get_contents('bd/employees.json');
	$employees = json_decode($employees);

	if (!is_null($args['min']) and !is_null($args['max'])) {
		$employees = array_filter($employees, function($emp) use ($args) {
			$salary = (float)preg_replace('/[$|,]/', '', $emp->salary);
			if ($salary >= (float)$args['min'] and $salary <= (float)$args['max']) {
				return $emp;
			}
		});
	}
	
	// Configuration header xml
	header('Content-Type: application/xml');
	// create obj xml and add child or items
	$xml = new SimpleXMLElement('<employees/>');
	foreach ($employees as $emp) {
		$item = $xml->addChild('employee');
		foreach ($emp as $idx => $val) {
			if (is_array($val)) {
				$child = $item->addChild($idx);
				foreach ($val as $value) {
					foreach ($value as $index => $v) {
						$child->addChild($index, $v);
					}
				}
			}
			$item->addChild($idx, $val);
		}
	}

	echo $xml->asXml();
    exit;
});


$app->run();

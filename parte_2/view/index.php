<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset="utf-8">
	<title>PHP - PARTE 2</title>
	<style type="text/css">
		body {
			font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size: 14px;
			line-height: 1.42857143;
			color: #333;
		}
		.table {
			border: 1px solid #ddd;
			width: 100%;
			max-width: 100%;
			margin-bottom: 20px;
			border-spacing: 0;
			border-collapse: collapse;
		}

		.table > thead > tr > th {
			border-bottom-width: 2px;
			border: 1px solid #ddd;
			vertical-align: bottom;
			padding: 8px;
			line-height: 1.42857143;
		}
		.table > tbody > tr > td {
			border: 1px solid #ddd;
			padding: 8px;
			line-height: 1.42857143;
			vertical-align: top;
		}

		.search {
			text-align: center;
			margin-bottom: 10px;
		}
		.search input {
			margin: auto;
			display: inline;
		}
		.search label {
			font-weight: bold;
			margin-right: 10px;
		}
		.btn {
			background-color: #ddd;
			left: 20px;
			padding: 10px;
			position: relative;
			box-shadow: none;
			border-radius: 5px;
			color: green;
			text-decoration: none;
			font-weight: bold;
		}
	</style>
</head>
<body>
<form action="/" method="POST">
	<div class="search">
		<label for="email">Email:</label>
		<input type="text" name="email" value="{{ email }}">
		<input type="submit" value="Buscar">
		<a href="/registrar" class="btn">Register Employee</a>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Position</th>
				<th>Salary</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			{% for item in employees %}
				<tr>
					<td>{{ item.name }}</td>
					<td>{{ item.email }}</td>
					<td>{{ item.position }}</td>
					<td>{{ item.salary }}</td>
					<td><a href="detalle/{{ item.id }}">Ver detalles</a></td>
				</tr>
			{% endfor %}
		</tbody>
	</table>
</form>
</body>
</html>
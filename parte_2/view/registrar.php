<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Registrar</title>
	<style type="text/css">
		.btn {
			background-color: #ddd;
			padding: 10px;
			position: relative;
			box-shadow: none;
			top: 15px;
			border-radius: 5px;
			color: green;
			text-decoration: none;
			font-weight: bold;
		}
		.centered {
			text-align: center;
		}
		.container {
			margin: auto;
			text-align: center;
		}
		form {
			margin: auto;
			display: block;
		}
		input, select {
			width: 250px;
		}
		form > div > label {
			text-align: left;
			display: inline-block;
			width: 100px;
			vertical-align: middle;
			padding: 10px;
		}
	</style>
</head>
<body>
<div>
	<h1 class="centered">Register Employee</h1>
	<div class="container">
		<form action="/registrarsave" method="POST">
			<div>
				<label>Is Online:</label>
				<select name="isOnline">
					<option value="S">SI</option>
					<option value="N">NO</option>
				</select>
			</div>
			<div>
				<label>Salary:</label>
				<input type="text" name="salary" placeholder="2500.52">
			</div>
			<div>
				<label>Age:</label>
				<input type="text" name="age" placeholder="23">
			</div>
			<div>
				<label>Position:</label>
				<input type="text" name="position" placeholder="developer">
			</div>
			<div>
				<label>Name:</label>
				<input type="text" name="name" placeholder="Marlon Inga Cahuana">
			</div>
			<div>
				<label>Gender:</label>
				<select name="gender">
					<option value="male">Male</option>
					<option value="female">Female</option>
				</select>
			</div>
			<div>
				<label>Email:</label>
				<input type="text" name="email" placeholder="marlonricslim@gmail.com">
			</div>
			<div>
				<label>Phone:</label>
				<input type="text" name="phone" placeholder="+051 983459377">
			</div>
			<div>
				<label>Address:</label>
				<input type="text" name="address" placeholder="Av. Tacna N° 250">
			</div>
			<div>
				<label>Skills:</label>
				<select multiple name="skills[]">
					{% for skill in skills %}
						<option value="{{ skill }}">{{ skill }}</option>
					{% endfor %}
				</select> 
			</div>
			<div>
				<button type="submit" class="btn">Registrar</button>
				<a href="/" class="btn">Regresar</a>
			</div>
		</form>
	</div>
</div>
</body>
</html>
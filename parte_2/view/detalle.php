<!DOCTYPE html>
<html lang="es">
<head>
	<title>Detalle Empleado</title>
	<style type="text/css">
		.pos-center {
			text-align: center;
			display: block;
		}
		.container {
			width: 100%;
			margin: auto;
		}
		table {
			margin: auto;
		}
		table > tbody > tr > td:nth-child(1) {
			padding: 10px 7px;
			font-weight: bold;
		}
		a {
			width: 200px;
			height: 50px;
			background-color: #ddd;
			padding: 10px;
			position: relative;
			top: 15px;
			border-radius: 5px;
			color: green;
			text-decoration: none;
			font-weight: bold;
		}
		h1 {
			color: blue;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<h1 class="pos-center">Detalle</h1>
	<div class="container">
		<table>
			<thead></thead>
			<tbody>
				{% for emp in employee %}
				<tr>
					<td>Name:</td>
					<td>{{ emp.name }}</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td>{{ emp.email }}</td>
				</tr>
				<tr>
					<td>Phone:</td>
					<td>{{ emp.phone }}</td>
				</tr>
				<tr>
					<td>Address:</td>
					<td>{{ emp.address }}</td>
				</tr>
				<tr>
					<td>Position:</td>
					<td>{{ emp.position }}</td>
				</tr>
				<tr>
					<td>Salary:</td>
					<td>{{ emp.salary }}</td>
				</tr>
				<tr>
					<td>Skills:</td>
					<td>
					{% for skill in emp.skills %}
						{{ skill.skill ~ ', ' }}
						{% if loop.last %}
							{{ skill.skill }}
						{% endif %}
					{% endfor %}
					</td>
				</tr>
					
				</tr>
				{% endfor %}
			</tbody>
		</table>
	</div>
	<div class="pos-center">
		<a href="/">Regresar</a>
	</div>
</body>
</html>